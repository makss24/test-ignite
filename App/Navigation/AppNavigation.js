import { StackNavigator } from 'react-navigation'
import AsciiScreen from '../Containers/AsciiScreen'
import LaunchScreen from '../Containers/LaunchScreen'

import styles from './Styles/NavigationStyles'

// Manifest of possible screens
const PrimaryNav = StackNavigator({
  AsciiScreen: { screen: AsciiScreen },
  LaunchScreen: { screen: LaunchScreen }
}, {
  // Default config for all screens
  headerMode: 'none',
  initialRouteName: 'AsciiScreen',
  navigationOptions: {
    headerStyle: styles.header
  }
})

export default PrimaryNav
